package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"time"
)

type Config struct {
	ListenAddress  string `json:"listenAddress"`
	UpdateCooldown int    `json:"updateCooldown"`
	HetznerDNS     struct {
		APIToken string `json:"apiToken"`
	} `json:"hetznerDNS"`
	DNSRecords map[string]DNSRecord `json:"dnsRecords"`
}

type DNSRecord struct {
	ZoneId       string `json:"zoneId"`
	RecordId     string `json:"recordId"`
	UpdaterToken string `json:"updaterToken"`
}

var config Config
var updateCooldown = map[string]int64{}

func updateHetznerRecord(zoneId string, recordId string, recordName string, ipAddress string) bool {
	json := []byte(`{"value": "` + ipAddress + `","ttl": 0,"type": "A","name": "` + recordName + `","zone_id": "` + zoneId + `"}`)
	body := bytes.NewBuffer(json)

	client := &http.Client{}
	req, err := http.NewRequest("PUT", "https://dns.hetzner.com/api/v1/records/"+recordId, body)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Auth-API-Token", config.HetznerDNS.APIToken)

	resp, err := client.Do(req)

	if err != nil {
		fmt.Println("Failure : ", err)
		return false
	}

	ioutil.ReadAll(resp.Body)
	return true
}

func requestHandler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/updateRecord":

		dnsRecordName := r.FormValue("record")

		if dnsRecord, ok := config.DNSRecords[dnsRecordName]; ok {
			updaterToken := r.FormValue("token")
			if updaterToken != dnsRecord.UpdaterToken {
				fmt.Fprintln(w, "invalid_token")
			} else {
				if time.Now().Unix()-updateCooldown[dnsRecordName] >= 10 {
					ipv4Address := r.FormValue("ip")

					if net.ParseIP(ipv4Address) == nil {
						fmt.Fprintln(w, "invalid_ip")
					} else {
						updateCooldown[dnsRecordName] = time.Now().Unix()

						if updateHetznerRecord(dnsRecord.ZoneId, dnsRecord.RecordId, dnsRecordName, ipv4Address) {
							fmt.Fprintln(w, "update_success")
						} else {
							fmt.Fprintln(w, "update_fail")
						}
					}
				} else {
					fmt.Fprintln(w, "rate_limit")
				}
			}
		} else {
			fmt.Fprintln(w, "invalid_record")
		}

	default:
		fmt.Fprintln(w, "invalid_request")
	}
}

func loadConfig(file string) error {
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return err
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&config)

	// loop through all dyndns zones and add them to dnsRecords
	for dnsRecordName, _ := range config.DNSRecords {
		// pre-define coolDown counter at zero for all zones
		updateCooldown[dnsRecordName] = 0
	}
	return nil
}

func main() {
	configFilePath := "config.json"

	configLoad := loadConfig(configFilePath)
	fmt.Println("[STARTUP] Loading configuration from file: " + configFilePath)

	if configLoad != nil {
		fmt.Println("[STARTUP] Could not load configuration file:")
		fmt.Println(configLoad.Error())
	} else {
		fmt.Println("[STARTUP] Successfully loaded configuration file!")
		fmt.Println("[STARTUP] Webserver is now listening for requests on '" + config.ListenAddress + "'")

		http.HandleFunc("/", requestHandler)
		http.ListenAndServe(config.ListenAddress, nil)
	}
}
