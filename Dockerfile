FROM golang:1.16.5-alpine

WORKDIR /opt
COPY . .

RUN go get -d -v .
RUN go install -ldflags="-s -w" -v .

EXPOSE 1811

CMD ["hetzner-ddns-api"]